import hilog from '@ohos.hilog';
import UIAbility from '@ohos.app.ability.UIAbility';

const TAG: string = 'DetailsAbility';
const KEY: string = 'GoodsId';
const DETAIL_ABILITY_DOMAIN = 0x00002;

/**
 * 商品详情页面
 */
export default class DetailsAbility extends UIAbility {

    onCreate(want, launchParam) {
        //获取传递过来的参数，并保持到本地存储中
        let goodsId: number = want?.parameters?.goodsId;
        AppStorage.SetOrCreate(KEY, goodsId);
        hilog.info(DETAIL_ABILITY_DOMAIN, TAG, '%{public}s', 'Ability onCreate');
    }

    onDestroy() {
        hilog.info(DETAIL_ABILITY_DOMAIN, TAG, '%{public}s', 'Ability onDestroy');
    }

    onWindowStageCreate(windowStage) {
        hilog.info(0x0000, TAG, '%{public}s', 'Ability onWindowStageCreate');
        //加载详情页面
        windowStage.loadContent('pages/DetailsPage', (err, data) => {
            if (err.code) {
                hilog.error(DETAIL_ABILITY_DOMAIN, TAG, 'Failed. Cause: %{public}s', JSON.stringify(err) ?? '');
                return;
            }
            hilog.info(DETAIL_ABILITY_DOMAIN, TAG, 'Succeeded. Data: %{public}s', JSON.stringify(data) ?? '');
        });
    }

    onWindowStageDestroy() {
        hilog.info(DETAIL_ABILITY_DOMAIN, TAG, '%{public}s', 'Ability onWindowStageDestroy');
    }

    onForeground() {
        hilog.info(DETAIL_ABILITY_DOMAIN, TAG, '%{public}s', 'Ability onForeground');
    }

    onBackground() {
        hilog.info(DETAIL_ABILITY_DOMAIN, TAG, '%{public}s', 'Ability onBackground');
    }
};
